/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"
	mocks "gitlab.com/paidit-se/mongo-mocks"
	wrappers "gitlab.com/paidit-se/mongo-wrappers"
	"go.mongodb.org/mongo-driver/v2/bson"
	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/mongo/options"

	"gitlab.com/unboundsoftware/sloth/model"
)

func TestMongo_Start(t *testing.T) {
	type fields struct {
		client func(t *testing.T) wrappers.Client
	}
	type args struct {
		handler func(model.Request) error
	}
	tests := []struct {
		name        string
		fields      fields
		quit        bool
		args        args
		wantErr     bool
		wantChanErr bool
		wantLogged  []string
	}{
		{
			name: "quit",
			fields: fields{
				client: func(t *testing.T) wrappers.Client {
					return &mocks.MockClient{
						Db: &mocks.MockDatabase{
							Coll: map[string]*mocks.MockCollection{
								"sloth": {
									FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
										return &mocks.MockCursor{}, nil
									},
								},
							},
						},
					}
				},
			},
			quit:    true,
			args:    args{},
			wantErr: false,
		},
		{
			name: "error fetching requests",
			fields: fields{
				client: func(t *testing.T) wrappers.Client {
					return &mocks.MockClient{
						Db: &mocks.MockDatabase{
							Coll: map[string]*mocks.MockCollection{
								"sloth": {
									FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
										want := bson.M{"until": bson.M{"$lte": time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)}}
										if !reflect.DeepEqual(filter, want) {
											t.Errorf("Start() got %s, want %s", litter.Sdump(filter), litter.Sdump(want))
										}
										return nil, errors.New("error")
									},
								},
							},
						},
					}
				},
			},
			args:        args{},
			wantErr:     false,
			wantChanErr: true,
			wantLogged: []string{
				"[ERROR] error fetching requests: error=error\n",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			errChan := make(chan error, 2)
			var chanErr error
			m := &Mongo{
				client:     tt.fields.client(t),
				database:   "db",
				collection: "sloth",
				timeSource: func() time.Time {
					return time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)
				},
				logger: logger,
			}
			if err := m.Start(model.HandlerFunc(tt.args.handler), model.ChannelQuitter(errChan)); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.quit {
				m.cancel()
			}
			<-m.ctx.Done()
			close(errChan)
			chanErr = <-errChan
			if (chanErr != nil) != tt.wantChanErr {
				t.Errorf("Start() error = %v, wantChanErr %v", chanErr, tt.wantChanErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestMongo_process(t *testing.T) {
	type fields struct {
		client wrappers.Client
	}
	type args struct {
		handler func(request model.Request) error
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "no requests",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
									return &mocks.MockCursor{}, nil
								},
							},
						},
					},
				},
			},
			args:       args{},
			wantErr:    false,
			wantLogged: nil,
		},
		{
			name: "error decoding request",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
									return &mocks.MockCursor{
										Objs: []string{`{"a":`},
									}, nil
								},
							},
						},
					},
				},
			},
			args:       args{},
			wantErr:    true,
			wantLogged: []string{"[ERROR] error decoding request: error=\"unexpected end of JSON input\"\n"},
		},
		{
			name: "error handling request",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
									return &mocks.MockCursor{
										Objs: []string{`{"_id":"5b178389121dfda51511044e","until":"2021-02-26T15:02:03.000Z","payload":{"target":"test://host/path","payload":"abc"}}`},
									}, nil
								},
							},
						},
					},
				},
			},
			args: args{
				handler: func(request model.Request) error {
					want := model.Request{
						DelayedUntil: time.Date(2021, 2, 26, 15, 2, 3, 0, time.UTC),
						Target:       "test://host/path",
						Payload:      []byte(`"abc"`),
					}
					if !reflect.DeepEqual(request, want) {
						t.Errorf("process() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
			},
			wantErr:    true,
			wantLogged: []string{"[ERROR] error handling request: error=error\n"},
		},
		{
			name: "error acknowledging request",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
									return &mocks.MockCursor{
										Objs: []string{`{"_id":"5b178389121dfda51511044e","until":"2021-02-26T15:02:03.000Z","payload":{"target":"test://host/path","payload":"abc"}}`},
									}, nil
								},
								DeleteOneFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error) {
									return nil, errors.New("error")
								},
							},
						},
					},
				},
			},
			args: args{
				handler: func(request model.Request) error {
					return nil
				},
			},
			wantErr:    false,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			litter.Config.HidePrivateFields = false
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Mongo{
				client:     tt.fields.client,
				database:   "db",
				collection: "sloth",
				timeSource: func() time.Time {
					return time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)
				},
				logger: logger,
			}
			if err := m.process(tt.args.handler); (err != nil) != tt.wantErr {
				t.Errorf("process() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestMongo_Stop(t *testing.T) {
	tests := []struct {
		name       string
		wantErr    bool
		wantLogged []string
	}{
		{
			name:       "stopping",
			wantErr:    false,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			ctx, cancel := context.WithCancel(context.Background())
			m := &Mongo{
				logger: logger,
				ctx:    ctx,
				cancel: cancel,
			}
			if err := m.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestMongo_Add(t *testing.T) {
	type fields struct {
		client     wrappers.Client
		database   string
		collection string
		timeSource func() time.Time
	}
	type args struct {
		request model.Request
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error inserting",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								InsertOneFunc: func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error) {
									want := bson.M{
										"until": time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
										"request": bson.M{
											"target":  "test://host/path",
											"payload": json.RawMessage("payload"),
										},
									}
									if !reflect.DeepEqual(document, want) {
										t.Errorf("Add() got %s, want %s", litter.Sdump(document), litter.Sdump(want))
									}
									return nil, errors.New("error")
								},
							},
						},
					},
				},
				database:   "db",
				collection: "sloth",
			},
			args: args{
				request: model.Request{
					DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
					Target:       "test://host/path",
					Payload:      []byte("payload"),
				},
			},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								InsertOneFunc: func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error) {
									return nil, nil
								},
							},
						},
					},
				},
				database:   "db",
				collection: "sloth",
			},
			args: args{
				request: model.Request{
					DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
					Target:       "test://host/path",
					Payload:      []byte("payload"),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Mongo{
				client:     tt.fields.client,
				database:   tt.fields.database,
				collection: tt.fields.collection,
				timeSource: tt.fields.timeSource,
			}
			if err := m.Add(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMongo_remove(t *testing.T) {
	id, _ := bson.ObjectIDFromHex("5b178389121dfda51511044e")
	type fields struct {
		client     wrappers.Client
		database   string
		collection string
		timeSource func() time.Time
	}
	type args struct {
		id bson.ObjectID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error removing",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								DeleteOneFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error) {
									want := bson.M{"_id": id}
									if !reflect.DeepEqual(filter, want) {
										t.Errorf("remove() got %s, want %s", litter.Sdump(filter), litter.Sdump(want))
									}
									return nil, errors.New("error")
								},
							},
						},
					},
				},
				database:   "db",
				collection: "sloth",
			},
			args:    args{id: id},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				client: &mocks.MockClient{
					Db: &mocks.MockDatabase{
						Coll: map[string]*mocks.MockCollection{
							"sloth": {
								DeleteOneFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error) {
									return nil, nil
								},
							},
						},
					},
				},
				database:   "db",
				collection: "sloth",
			},
			args:    args{id: id},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Mongo{
				client:     tt.fields.client,
				database:   tt.fields.database,
				collection: tt.fields.collection,
				timeSource: tt.fields.timeSource,
			}
			if err := m.remove(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("remove() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_storedRequest_Request(t *testing.T) {
	type fields struct {
		Until   time.Time
		Payload struct {
			Target  string          `bson:"target" json:"target"`
			Payload json.RawMessage `bson:"payload" json:"payload"`
		}
	}
	tests := []struct {
		name   string
		fields fields
		want   model.Request
	}{
		{
			name: "success",
			fields: fields{
				Until: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
				Payload: struct {
					Target  string          `bson:"target" json:"target"`
					Payload json.RawMessage `bson:"payload" json:"payload"`
				}{
					Target:  "test://host/path",
					Payload: json.RawMessage("abc"),
				},
			},
			want: model.Request{
				DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
				Target:       "test://host/path",
				Payload:      json.RawMessage("abc"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := storedRequest{
				Until:   tt.fields.Until,
				Payload: tt.fields.Payload,
			}
			if got := s.Request(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Request() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMongo_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		connect    func(t *testing.T) func(ctx context.Context, mongoUrl string) (wrappers.Client, error)
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "connection failure",
			args: []string{"--mongo-url", "mongodb://user:password@127.0.0.1:27017/db?authSource=admin"},
			connect: func(t *testing.T) func(ctx context.Context, mongoUrl string) (wrappers.Client, error) {
				return func(ctx context.Context, mongoUrl string) (wrappers.Client, error) {
					assert.Equal(t, "mongodb://user:password@127.0.0.1:27017/db?authSource=admin", mongoUrl)
					return nil, fmt.Errorf("connection error")
				}
			},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "connection error")
			},
			wantLogged: nil,
		},
		{
			name: "success",
			args: []string{"--mongo-url", "mongodb://user:password@127.0.0.1:27017/db?authSource=admin/"},
			connect: func(t *testing.T) func(ctx context.Context, mongoUrl string) (wrappers.Client, error) {
				return func(ctx context.Context, mongoUrl string) (wrappers.Client, error) {
					return nil, nil
				}
			},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Mongo{
				logger: logger,
			}
			if tt.connect != nil {
				connectToMongoFunc = tt.connect(t)
			}
			got, err := m.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestMongo_IsSource(t *testing.T) {
	assert.Equalf(t, false, (&Mongo{}).IsSource(), "IsSource()")
}

func TestMongo_IsSink(t *testing.T) {
	assert.Equalf(t, false, (&Mongo{}).IsSink(), "IsSink()")
}

func TestMongo_IsStore(t *testing.T) {
	assert.Equalf(t, true, (&Mongo{}).IsStore(), "IsStore()")
}
