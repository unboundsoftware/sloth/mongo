/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"context"
	"encoding/json"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/alecthomas/kong"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	wrappers "gitlab.com/paidit-se/mongo-wrappers"
	"go.mongodb.org/mongo-driver/v2/bson"
	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/mongo/options"

	"gitlab.com/unboundsoftware/sloth/model"
	mp "gitlab.com/unboundsoftware/sloth/model/plugin"
)

type Config struct {
	MongoUrl   *url.URL `name:"mongo-url" env:"MONGO_URL" help:"Url to MongoDB server to use for store"`
	Database   string   `name:"mongo-database" env:"MONGO_DATABASE" help:"Database where requests will be stored."`
	Collection string   `name:"mongo-collection" env:"MONGO_COLLECTION" default:"sloth" help:"Collection where requests will be stored."`
}

type Mongo struct {
	client     wrappers.Client
	database   string
	collection string
	timeSource func() time.Time
	logger     hclog.Logger
	mutex      sync.Mutex
	ctx        context.Context
	cancel     context.CancelFunc
}

func (m *Mongo) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("mongo_store is a pluggable store for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if cli.MongoUrl == nil {
		return false, nil
	}

	client, err := connectToMongoFunc(context.Background(), cli.MongoUrl.String())
	if err != nil {
		return false, err
	}
	m.client = client
	m.database = cli.Database
	m.collection = cli.Collection
	m.timeSource = time.Now

	return true, nil
}

func (m *Mongo) IsSource() bool {
	return false
}

func (m *Mongo) IsSink() bool {
	return false
}

func (m *Mongo) IsStore() bool {
	return true
}

func (m *Mongo) Start(handler model.Handler, quitter model.Quitter) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	go m.loop(handler.Handle, quitter)
	ctx, cancel := context.WithCancel(context.Background())
	m.ctx = ctx
	m.cancel = cancel
	return nil
}

func (m *Mongo) loop(handler func(model.Request) error, quitter model.Quitter) {
	defer func() {
		m.mutex.Lock()
		defer m.mutex.Unlock()
		m.cancel()
	}()
	m.mutex.Lock()
	defer m.mutex.Unlock()
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-m.ctx.Done():
			return
		case <-ticker.C:
			if err := m.process(handler); err != nil {
				quitter.Quit(err)
				return
			}
		}
	}
}

func (m *Mongo) process(handler func(request model.Request) error) error {
	now := m.timeSource()
	ctx := context.Background()
	cursor, err := m.client.
		Database(m.database).
		Collection(m.collection).
		Find(ctx, bson.M{
			"until": bson.M{
				"$lte": now,
			},
		})
	if err != nil {
		m.logger.Error("error fetching requests", "error", err)
		return err
	}
	for cursor.Next(ctx) {
		request := &storedRequest{}
		err := cursor.Decode(request)
		if err != nil {
			m.logger.Error("error decoding request", "error", err)
			return err
		}
		request.store = m
		if err := handler(request.Request()); err == nil {
			_ = request.Ack()
		} else {
			m.logger.Error("error handling request", "error", err)
			return err
		}
	}
	return nil
}

func (m *Mongo) Stop() error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.logger.Info("stopping")
	if m.ctx != nil {
		m.cancel()
	}
	return nil
}

func (m *Mongo) Add(request model.Request) error {
	_, err := m.client.
		Database(m.database).
		Collection(m.collection).
		InsertOne(context.Background(), bson.M{
			"until": request.DelayedUntil,
			"request": bson.M{
				"target":  request.Target,
				"payload": request.Payload,
			},
		})
	return err
}

func (m *Mongo) remove(id bson.ObjectID) error {
	_, err := m.client.Database(m.database).Collection(m.collection).DeleteOne(context.Background(), bson.M{"_id": id})
	return err
}

var _ model.Store = &Mongo{}

type storedRequest struct {
	store   *Mongo
	ID      bson.ObjectID `bson:"_id" json:"_id"`
	Until   time.Time     `bson:"until" json:"until"`
	Payload struct {
		Target  string          `bson:"target" json:"target"`
		Payload json.RawMessage `bson:"payload" json:"payload"`
	} `bson:"request" json:"payload"`
}

func (s storedRequest) Request() model.Request {
	request := model.Request{
		DelayedUntil: s.Until,
		Target:       s.Payload.Target,
		Payload:      s.Payload.Payload,
	}
	return request
}

func (s storedRequest) Ack() error {
	return s.store.remove(s.ID)
}

var connectToMongoFunc = connectToMongo

func connectToMongo(ctx context.Context, mongoUrl string) (wrappers.Client, error) {
	c, err := mongo.Connect(options.Client().ApplyURI(mongoUrl))
	return &wrappers.ClientW{Client: c}, err
}

func main() {
	logger := hclog.Default()
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: mp.Handshake,
		Plugins: map[string]plugin.Plugin{
			"store": &mp.StorePlugin{
				Impl: &Mongo{logger: logger},
			},
		},
		GRPCServer: plugin.DefaultGRPCServer,
	})
}
